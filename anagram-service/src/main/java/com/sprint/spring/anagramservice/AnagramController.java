/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring.anagramservice;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author avincze
 */
@RestController
public class AnagramController {
    @GetMapping(path = "/anagram/{word}")
    public String findAnagram(@PathVariable(name = "word") String word){
        if ("secure".equals(word)) {
            return "secure;rescue";
        } else {
            return "";
        }
    } 
}