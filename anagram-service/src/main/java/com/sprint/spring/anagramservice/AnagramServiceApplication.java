package com.sprint.spring.anagramservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class AnagramServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnagramServiceApplication.class, args);
	}
}
