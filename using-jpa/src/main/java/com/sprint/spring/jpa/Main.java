package com.sprint.spring.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class Main {

    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory
                = Persistence.createEntityManagerFactory("bookdbPU2");
        EntityManager entityManager
                = entityManagerFactory.createEntityManager();
        EntityTransaction transaction
                = entityManager.getTransaction();
        transaction.begin();
        Book book1 = new Book();
        book1.setName("Book 1");
        Book book2 = new Book();
        book2.setName("Book 2");
        entityManager.persist(book1);
        entityManager.persist(book2);

        transaction.commit();
        entityManager.close();
        entityManagerFactory.close();
    }

}
