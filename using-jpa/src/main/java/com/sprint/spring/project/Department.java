package com.sprint.spring.project;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;

@Entity
public class Department extends EricssonEntity {

    @Basic(optional = false)
    @Column(name = "name")
    private String name;

    public Department() {
        // TODO Auto-generated constructor stub
        this(null);
    }

    public Department(String name) {
        // TODO Auto-generated constructor stub
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
