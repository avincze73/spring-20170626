/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring.anagramclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author avincze
 */
@RestController
public class ClientController {
    
    @Autowired
    private AnagramFeignClient anagramFeignClient;
    
    @GetMapping(path = "/feign/{word}")
    public String getAnagram(@PathVariable("word") String word){
        String ret = anagramFeignClient.find(word);
        return ret;
    }
}
