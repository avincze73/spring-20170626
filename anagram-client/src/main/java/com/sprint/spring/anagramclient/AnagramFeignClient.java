/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring.anagramclient;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author avincze
 */
@FeignClient(name = "anagram-service")
public interface AnagramFeignClient {

    @RequestMapping(value = "/anagram/{word}", method = RequestMethod.GET)
    public String find(@PathVariable("word") String word);
}
