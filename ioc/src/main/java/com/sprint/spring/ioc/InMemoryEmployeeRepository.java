package com.sprint.spring.ioc;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class InMemoryEmployeeRepository implements EmployeeRepository {

	private Set<Employee> database = new HashSet<>();
	
	@Override
	public void add(Employee employee) {
		// TODO Auto-generated method stub
		database.add(employee);
	}

	@Override
	public List<Employee> getAll() {
		// TODO Auto-generated method stub
		return new ArrayList(database);
	}

	public Set<Employee> getDatabase() {
		return database;
	}

	public void setDatabase(Set<Employee> database) {
		this.database = database;
	}
	
	

}
