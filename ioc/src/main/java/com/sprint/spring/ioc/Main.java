package com.sprint.spring.ioc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
	public static void main(String[] args) {
		ApplicationContext context = 
				new ClassPathXmlApplicationContext("beans.xml");
		EmployeeService employeeService = 
				context.getBean("employeeService", 
						EmployeeService.class);
		Employee employee = new Employee();
		employee.setId(6);
		employee.setFirstName("firstName6");
		employeeService.add(employee);
		employeeService.findAll().forEach(System.out::println);
	}
}
