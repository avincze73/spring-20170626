package com.sprint.spring.ioc;

import java.util.List;

public class EmployeeService {
	private EmployeeRepository repository;

	public EmployeeRepository getRepository() {
		return repository;
	}

	public void setRepository(EmployeeRepository repository) {
		this.repository = repository;
	}

	public void add(Employee employee) {
		repository.add(employee);
	}

	public List<Employee> findAll() {
		return repository.getAll();
	}
}
