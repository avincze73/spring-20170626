package com.sprint.spring.ioc;

import java.util.List;

public interface EmployeeRepository {
	void add(Employee employee);

	List<Employee> getAll();
}
