/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import static org.hamcrest.Matchers.is;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.BDDMockito.given;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import org.springframework.restdocs.payload.JsonFieldType;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import org.springframework.restdocs.request.ParameterDescriptor;
import org.springframework.restdocs.request.RequestDocumentation;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 *
 * @author avincze
 */
@RunWith(SpringRunner.class)
@WebMvcTest(AccountController.class)
@AutoConfigureRestDocs(outputDir = "target/snippets")
public class AccountRestDocsTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private IAccountService accountService;

    @MockBean
    private AccountRepository accountRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Before
    public void setup() {
        given(this.accountService.getByUserName("u1")).willReturn(new Account("u1", "pw", "u 1"));
    }

    @Test
    public void test3() throws Exception {
        Map<String, String> account = new HashMap<>();
        account.put("username", "un4");
        account.put("password", "pw4");
        account.put("fullname", "un 4");

        this.mvc.perform(
                post("/account/add").contentType(MediaType.APPLICATION_JSON).content(
                        this.objectMapper.writeValueAsString(account)
                )
        ).andExpect(status().isCreated())
                .andDo(
                        document("addaccount", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint()),
                                //responseFields(),
                                
                                requestFields(
                                        fieldWithPath("username").description("User name"),
                                        fieldWithPath("password").description("Password"),
                                        fieldWithPath("fullname").description("Full name").type(JsonFieldType.STRING).optional()
                                )));
        
    }
    
    
    // @Test
//    public void test4() throws Exception {
//        Map<String, String> account = new HashMap<>();
//        account.put("username", "un4");
//        account.put("password", "pw4");
//        account.put("fullname", "un 4");
//
//        this.mvc.perform(
//                post("/account/test").contentType(MediaType.APPLICATION_JSON).content(
//                        this.objectMapper.writeValueAsString(account)
//                )
//        ).andExpect(status().isCreated())
//                .andDo(
//                        document("addaccount", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint()),
//                                //responseFields(),
//                                
//                                requestFields(
//                                        fieldWithPath("username").description("User name"),
//                                        fieldWithPath("password").description("Password"),
//                                        fieldWithPath("fullname").description("Full name").type(JsonFieldType.STRING).optional()
//                                )));
//        
//    }
    
    
    

    @Test
    public void test2() throws Exception {
        ArrayList<Account> accountList = new ArrayList<>();
        accountList.add(new Account("u1", "pw", "u 1"));
        accountList.add(new Account("u2", "pw", "u 2"));

        given(this.accountService.getAll()).willReturn(accountList);

        this.mvc.perform(get("/accounts", "").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(
                        document("allaccounts", preprocessRequest(prettyPrint()), 
                                preprocessResponse(prettyPrint()),
                                RequestDocumentation.requestParameters(),
                                responseFields(
                                        fieldWithPath("[].username").description("User name"),
                                        fieldWithPath("[].password").description("Password"),
                                        fieldWithPath("[].fullname").description("Full name")
                                                .type(JsonFieldType.STRING).optional()
                                )));
    }

    @Test
    public void test1() throws Exception {

        this.mvc.perform(get("/account/{username}", "u1").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.username", is("u1")))
                .andDo(
                        document("findaccount", 
                                preprocessRequest(prettyPrint()), 
                                preprocessResponse(prettyPrint()),
                                RequestDocumentation.requestParameters(),
                                //pathParameters(parameterWithName("username").description("user name")),
                                //pathParameters(userPathParams()),
                                responseFields(
                                        fieldWithPath("username").description("User name"),
                                        fieldWithPath("password").description("Password"),
                                        fieldWithPath("fullname").description("Full name").type(JsonFieldType.STRING).optional()
                                )));

    }

    private static ParameterDescriptor[] userPathParams() {
        return new ParameterDescriptor[]{
            parameterWithName("username").description("Username")
        };
    }
}
