/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring;

import org.assertj.core.api.AbstractBooleanAssert;
import org.assertj.core.api.Assertions;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.BDDMockito.given;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author avincze
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
//@SpringBootTest
public class AccountIntegrationTestsWithMock {

    @Autowired
    private TestRestTemplate restTemplate;

    @MockBean
    private IAccountService accountService;

    @LocalServerPort
    private int port;

    @Before
    public void setup() {
        given(this.accountService.getByUserName("u1")).willReturn(new Account("u1", "pw", "u 1"));
    }

    @Test
    public void test1() {
        //System.out.println(port);
        ResponseEntity<Account> account = this.restTemplate.getForEntity("/account/u1", Account.class);
        assertThat(account.getBody().getUsername()).isEqualTo("u1");
    }
    
    @Test
    public void test2() {
        //System.out.println(port);
        ResponseEntity<Account> account = this.restTemplate.getForEntity("/accounts", Account.class);
        assertThat(account.getBody().getUsername()).isEqualTo("u1");
    }
}
