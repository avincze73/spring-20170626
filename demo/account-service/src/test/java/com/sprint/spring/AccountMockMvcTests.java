/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.assertj.core.api.AbstractBooleanAssert;
import org.assertj.core.api.Assertions;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.BDDMockito.given;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
/**
 *
 * @author avincze
 */
@RunWith(SpringRunner.class)
@WebMvcTest(AccountController.class)
//@SpringBootTest(classes = AccountController.class)
public class AccountMockMvcTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private IAccountService accountService;

    @MockBean
    private AccountRepository accountRepository;
    

    @Before
    public void setup() {
        given(this.accountService.getByUserName("u1")).willReturn(new Account("u1", "pw", "u 1"));
    }

    @Test
    public void test() {
        try {
            this.mvc.perform(get("/account/u1").accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk())
                    .andExpect(jsonPath("$.username", is("u1")));
        } catch (Exception ex) {
            Logger.getLogger(AccountMockMvcTests.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
