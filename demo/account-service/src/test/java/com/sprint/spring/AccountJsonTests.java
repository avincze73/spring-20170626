/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.json.JacksonTester;

/**
 *
 * @author avincze
 */
public class AccountJsonTests {
    private JacksonTester<Account> json;

    @Before
    public void setup() {
        ObjectMapper objectMapper = new ObjectMapper(); 
        JacksonTester.initFields(this, objectMapper);
    }

    @Test
    public void serializeJson() {
        try {
            Account account = new Account("u1", "pw1", "u 1");
            
            assertThat(this.json.write(account)).isEqualToJson("account.json");
            
            assertThat(this.json.write(account)).hasJsonPathStringValue("@.username");
            
            assertThat(this.json.write(account)).extractingJsonPathStringValue("@.password").isEqualTo("pw1");
        } catch (IOException ex) {
            Logger.getLogger(AccountJsonTests.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void deserializeJson() {
        try {
            String content = "{\"username\":\"u1\",\"password\":\"pw1\",\"fullname\":\"u 1\"}";
            
            //assertThat(this.json.parse(content)).isEqualTo(new  Account("u1", "pw1", "u 1"));
            
            assertThat(this.json.parseObject(content).getUsername()).isEqualTo("u1");
        } catch (IOException ex) {
            Logger.getLogger(AccountJsonTests.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
