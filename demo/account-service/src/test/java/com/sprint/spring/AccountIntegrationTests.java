/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring;

import org.assertj.core.api.AbstractBooleanAssert;
import org.assertj.core.api.Assertions;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author avincze
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
//@SpringBootTest
public class AccountIntegrationTests {
    @Autowired
    private TestRestTemplate restTemplate;
    
    
    @LocalServerPort
    private int port;

    @Test
    public void test() {
        System.out.println(port);
        ResponseEntity<Account> account = this.restTemplate.getForEntity("/account/user2", Account.class);
        assertThat(account.getBody().getUsername()).isEqualTo("user2");
    }
}
