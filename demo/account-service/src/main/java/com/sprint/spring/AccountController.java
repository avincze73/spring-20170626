/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author avincze
 */
@RestController
public class AccountController {

    //@Autowired
    //private AccountRepository accountRepository;
    @Autowired
    private IAccountService accountService;

    @RequestMapping(value = "/account/{username}", method = RequestMethod.GET)
    public ResponseEntity<?> findAccount(@PathVariable("username") String username) {
        Account account = accountService.getByUserName(username);
        if (account == null) {
            return new ResponseEntity(new CustomErrorType("Account with id " + username
                    + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(account, HttpStatus.OK);
    }

    @RequestMapping(value = "/accounts", method = RequestMethod.GET)
    public ResponseEntity<List<Account>> listAllAccounts() {
        List<Account> accounts = accountService.getAll();
        if (accounts.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(accounts, HttpStatus.OK);
    }

    @RequestMapping(value = "/account/add", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void createAccount(@RequestBody Account account) {
        accountService.save(new Account(account.getUsername(), account.getPassword(), account.getFullname()));
    }
    
    @RequestMapping(value = "/account/test", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void createAccount1(@RequestBody Account account, @RequestBody Account account2) {
        
    }
}
