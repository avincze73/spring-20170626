/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring;

import java.util.List;

/**
 *
 * @author avincze
 */
public interface IAccountService {
    Account getByUserName(String userName);
    List<Account> getAll();
    void save(Account account);
    
}
