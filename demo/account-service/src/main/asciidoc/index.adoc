= First REST Service API Guide
:doctype: book
:icons: font
:source-highlighter: highlightjs
:toc: left
:toclevels: 4
:sectlinks:
 
= Resources
 
== Account REST Service
 
The Greeting provides the entry point into the service.
 
=== Finding account
 
==== Request structure
 
include::{snippets}/findaccount/http-request.adoc[]
 
==== Request parameters
 
include::{snippets}/findaccount/request-parameters.adoc[]
 
==== Response fields
 
include::{snippets}/findaccount/response-fields.adoc[]
 
==== Example response
 
include::{snippets}/findaccount/http-response.adoc[]
 
==== CURL request
 
include::{snippets}/findaccount/curl-request.adoc[]
 

=== All accounts
 
==== Request structure
 
include::{snippets}/allaccounts/http-request.adoc[]
 
==== Request parameters
 
include::{snippets}/allaccounts/request-parameters.adoc[]
 
==== Response fields
 
include::{snippets}/allaccounts/response-fields.adoc[]
 
==== Example response
 
include::{snippets}/allaccounts/http-response.adoc[]
 
==== CURL request
 
include::{snippets}/allaccounts/curl-request.adoc[]



=== Add account
 
==== Request structure
 
include::{snippets}/addaccount/http-request.adoc[]
 
==== Request parameters
 
include::{snippets}/addaccount/request-parameters.adoc[]
 
==== CURL request
 
include::{snippets}/addaccount/curl-request.adoc[]



=== Add account 2
 
==== Request structure
 
include::{snippets}/addaccount2/http-request.adoc[]
 
==== Request parameters
 
include::{snippets}/addaccount2/request-parameters.adoc[]
 
==== CURL request
 
include::{snippets}/addaccount2/curl-request.adoc[]