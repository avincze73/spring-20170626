package com.sprint.spring.anagram.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Peter on 2017. 03. 18..
 */
public class EurekaProxyService {
    private final String instanceId;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private DiscoveryClient discoveryClient;

    public EurekaProxyService(String instanceId) {
        this.instanceId = instanceId;
    }

    public <T> ResponseEntity<T> call(String url, HttpMethod method, HttpEntity<String> request, Class<T> clazz) {
        String urlPrefix = discoveryClient.getInstances(instanceId).get(0).getUri().toString();
        return restTemplate.exchange(urlPrefix + url, method, request, clazz);
    }
}
