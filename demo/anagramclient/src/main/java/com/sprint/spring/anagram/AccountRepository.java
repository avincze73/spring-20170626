package com.sprint.spring.anagram;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository 
        extends JpaRepository<Account, String> {

    Account findByUsername(String userName);
}
