package com.sprint.spring.anagram.controller;

import com.sprint.spring.anagram.App;
import com.sprint.spring.anagram.service.EurekaProxyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

import static com.sprint.spring.anagram.auth.AuthService.ACC_SERVICE_CLOUD_URL;
import static org.springframework.http.HttpMethod.GET;

/**
 * Created by peter.gergics on 2017. 03. 17..
 */
@Controller
public class AnagramController {
    @Autowired
    @Qualifier("anagramProxyService")
    private EurekaProxyService anagramProxyService;

    @GetMapping("/")
    public String index() {
        return "redirect:anagram";
    }

    @GetMapping("/regist")
    public String regist() {
        return "regist";
    }

    @GetMapping("/anagram")
    public String anagram() {
        return "anagram";
    }

    @GetMapping("/anagram/number-of-characters")
    public String numberOfCharacters(Model model) {
        HttpEntity<String> request = new HttpEntity<>(App.getAuthHttpHeader());
        ResponseEntity<Integer> response = anagramProxyService.call("/api/number-of-characters", GET, request, Integer.class);
        model.addAttribute("numberOfCharacters", response.getBody());
        return "anagram";
    }

    @GetMapping("/anagram/frequency-of-lengths")
    public String frequencyOfLengths(Model model) {
        HttpEntity<String> request = new HttpEntity<>(App.getAuthHttpHeader());
        ResponseEntity<Map> response = anagramProxyService.call("/api/frequency-of-lengths", GET, request, Map.class);
        model.addAttribute("frequencyOfLengths", response.getBody());
        return "anagram-frequency-of-lengths";
    }

    @GetMapping("/anagram/frequency-of-characters")
    public String frequencyOfCharacters(Model model) {
        HttpEntity<String> request = new HttpEntity<>(App.getAuthHttpHeader());
        ResponseEntity<Map> response = anagramProxyService.call("/api/frequency-of-characters", GET, request, Map.class);
        model.addAttribute("frequencyOfCharacters", response.getBody());
        return "anagram-frequency-of-characters";
    }

    @GetMapping("/anagram/length-of-longest")
    public String lengthOfLongest(Model model) {
        HttpEntity<String> request = new HttpEntity<>(App.getAuthHttpHeader());
        ResponseEntity<Integer> response = anagramProxyService.call("/api/length-of-longest", GET, request, Integer.class);
        model.addAttribute("lengthOfLongest", response.getBody());
        return "anagram";
    }

    @GetMapping("/anagram/length-of-shortest")
    public String lengthOfShortest(Model model) {
        HttpEntity<String> request = new HttpEntity<>(App.getAuthHttpHeader());
        ResponseEntity<Integer> response = anagramProxyService.call("/api/length-of-shortest", GET, request, Integer.class);
        model.addAttribute("lengthOfShortest", response.getBody());
        return "anagram";
    }

    @GetMapping("/anagram/average-of-lengths")
    public String averageOfLengths(Model model) {
        HttpEntity<String> request = new HttpEntity<>(App.getAuthHttpHeader());
        ResponseEntity<Double> response = anagramProxyService.call("/api/average-of-lengths", GET, request, Double.class);
        model.addAttribute("averageOfLengths", response.getBody());
        return "anagram";
    }

    @GetMapping("/anagram/anagram-groups/{wordLength}")
    public String anagramGroups(@PathVariable int wordLength, Model model) {
        HttpEntity<String> request = new HttpEntity<>(App.getAuthHttpHeader());
        ResponseEntity<Map> response = anagramProxyService.call("/api/anagram-groups/" + wordLength, GET, request, Map.class);
        model.addAttribute("anagramGroups", response.getBody());
        return "anagram";
    }

    @PostMapping("/anagram")
    public String anagramInput(String input, Model model) {
        if (input == null || input.length() == 0) {
            return "redirect:anagram";
        }
        HttpEntity<String> request = new HttpEntity<>(App.getAuthHttpHeader());
        ResponseEntity<List> response = anagramProxyService.call("/api/find-anagrams/" + input, GET, request, List.class);
        model.addAttribute("anagrams", response.getBody());
        return "anagram";
    }

    @PostMapping("/reg-user")
    public String regUser(String username, String password) {
        HttpEntity<String> request = new HttpEntity<>(App.getAuthHttpHeader());
        String url = ACC_SERVICE_CLOUD_URL + "add/" + username + "/" + password;
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.exchange(url, GET, request, String.class);
        return "redirect:login";
    }
}
