package com.sprint.spring.anagram.auth;

import com.sprint.spring.anagram.Account;
import com.sprint.spring.anagram.AccountRepository;
import com.sprint.spring.anagram.App;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import static org.springframework.http.HttpMethod.GET;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Created by peter.gergics on 2017. 03. 14..
 */
@Service
public class AuthService implements UserDetailsService {
    public static final String ACC_SERVICE_URL = "http://localhost:8091/account/";
    public static final String ACC_SERVICE_CLOUD_URL = "https://account-service-gergics.cfapps.io/account/";

    /*@Autowired
    private RestTemplate restTemplate;*/
    
    @Autowired
    private AccountRepository accountRepository;

    private final Logger logger = 
            LoggerFactory.getLogger(this.getClass());

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        HttpEntity<String> request = new HttpEntity<>(App.getAuthHttpHeader());
//        String url = ACC_SERVICE_CLOUD_URL + username;
//        RestTemplate restTemplate = new RestTemplate();
//        ResponseEntity<String> response = restTemplate.exchange(url, GET, request, String.class);
//        String hashedPassword = response.getBody();
//
//        if (hashedPassword == null) {
//            throw new UsernameNotFoundException("Cannot found user with username: " + username);
//        }

        List<GrantedAuthority> authorities = new ArrayList<>();
        SimpleGrantedAuthority roleAdmin = new SimpleGrantedAuthority("ROLE_ADMIN");
        authorities.add(roleAdmin);

        //return new User(username, hashedPassword, authorities);
        Account account = accountRepository.findByUsername(username);
        if (account == null) {
            throw new UsernameNotFoundException("No user");
        }
        
        logger.info("mylog: " + username);
        return new User(username, account.getPassword(), authorities);
    }
}
