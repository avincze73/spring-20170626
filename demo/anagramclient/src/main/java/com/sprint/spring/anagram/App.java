package com.sprint.spring.anagram;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;

/**
 * Created by peter.gergics on 2017. 03. 14..
 */
@ComponentScan
//@EnableDiscoveryClient
@SpringBootApplication
public class App {
    public static String ANAGRAM_SERVICE_ID = "anagram-service";
    private static String CRED = "sprint:sprint";

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    public static HttpHeaders getAuthHttpHeader() {
        String plainCreds = CRED;
        byte[] plainCredsBytes = plainCreds.getBytes();
        byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
        String base64Creds = new String(base64CredsBytes);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Creds);
        return headers;
    }
    
    @Bean
    CommandLineRunner init(AccountRepository accountRepository) {
        return (args) -> {
            Account a1 = new Account();
            a1.setUsername("user1");
            a1.setPassword("password");
            a1.setFullname("user 1");
            accountRepository.save(a1);

            a1 = new Account();
            a1.setUsername("user2");
            a1.setPassword("password");
            a1.setFullname("user 2");
            accountRepository.save(a1);
        };
    }
}
