package com.sprint.spring.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sprint.spring.web.domain.User;

import javax.validation.Valid;

@Controller
public class UserController {

    @RequestMapping(value = "/form.mvc")
    public ModelAndView user() {
        return new ModelAndView("userForm", "user", new User());
    }

    @RequestMapping(value = "/result.mvc")
    public ModelAndView processUser(@Valid User user, BindingResult result) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("u", user);

        if (result.hasErrors()) {
            modelAndView.setViewName("userForm");
        } else {
            modelAndView.setViewName("userResult");
            //modelAndView.setViewName("userForm");
        }

        return modelAndView;
    }
}
